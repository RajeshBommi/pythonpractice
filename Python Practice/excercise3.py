

planets= ["Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune"]
print(planets)
print(len(planets))
planets.append("pluto")
print(planets)
print(planets[-1],"is the last planet")

numbers=[1,2,3,8,9,4,56,34,55]
num=4
print("Maximum",num*max(numbers))
print("Minimum",num*min(numbers))
# slice A slice creates a new list. It doesn't modify the current list.
print(numbers[0:2])

s = "Hello, World!"
print(s[::-1])  # Output: "!dlroW ,olleH"


# Joining lists creates a new list. It doesn't modify the current list.
nums1=[1,3]
nums2=[2,4]
nums3=nums1+nums2
print(nums3)
nums3.sort()
print(nums3)