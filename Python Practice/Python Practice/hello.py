print("hello world")
print("hi")

x=True
print(type(x))

x="String"
print(x)
print(type(x))

x="Hello"+" "+"World"
print(x)

# name=input('Enter your name:')
# print(name)

# print("What is your name")
# name=input()
# print(name)

# Reading numbers as input
number=input("Enter a number- ")
print(type(number))

# To turn the value into a true integer variable, you can use the int() function:
num=int(input("Enter a number "))
print(type(num))

# Converting numbers to strings
x=5
print("The number is "+str(x))