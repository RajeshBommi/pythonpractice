mars_temperature = "The highest temperature on Mars is about 30 C"
for item in mars_temperature.split():
    if item.isnumeric():
        print(item)

        print("-60".startswith("-"))
        print("temp".endswith("p"))
        print("temp is high".replace("temp","temperature"))

        text="Temperature are very high"
        print("temperatures" in text)

        text1="Temperature are very high."
        print("temperature" in text1.lower())

        text2="Temp is very"
        print(" ".join([text2,"high"]))

        mass="1/6"
        print("on moon %s mass percentage"%mass)

        mass1='1/6'
        print("on moon {} percent of weight".format(mass1))

        mass_percentage = "1/6"
print("""You are lighter on the {0}, because on the {0} you would weigh about {1} of your weight on Earth.""".format("Moon", mass_percentage))

count=[4,3,2,1,0]
for num in count:
    print(num)

    from time import sleep

countdown = [4, 3, 2, 1, 0]

for number in countdown:
    print(number)
    sleep(1)  # Wait 1 second
print("Blast off!! 🚀")