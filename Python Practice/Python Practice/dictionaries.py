
planet={
    'name':'Earth',
    'moons':1
}

print(planet.get("name"))

planet.update({
    'name':'Jupiter',
    'moons':79
})
planet['orbital']=4900
planet.pop('orbital')
print(planet)

planet['address']={
    'polar':32,
    'equi':1432
}
print(planet)

raifall={
    'october': 3.5,
    'november': 4.2,
    'december': 2.1
}

for key in raifall.keys():
    print(f'{key}:{raifall[key]}cm')

total=0
for value in raifall.values():
    total=total+value
print(f'There was {total} rainfall')
