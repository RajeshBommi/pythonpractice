fact="Sun rises in the east"
print(fact)

fact="Sun rises"
two_fact=fact+" in the east"
print(two_fact)

mutliline="Facts about Sun: \n It is in red color \n Rises in the east "
print(mutliline)

mutliline="""Facts about Sun: 
It is in red color. 
Rises in the east. """
print(mutliline)


print("temperatures and facts about the moon".title())

temperatures = "Daylight: 260 F Nighttime: -280 F"
tem_split=temperatures.split()
print(tem_split)

temperatures2 = "Daylight: 260 F\n Nighttime: -280 F"
tem_split2=temperatures2.split("\n")
print(tem_split2)

#  print("Moon" in "Moon rises in the east") true
#  print("Moon" in "moon rises in the east") false

temperatures = """Saturn has a daytime temperature of -170 degrees Celsius, while Mars has -28 Celsius."""
print(temperatures.find("Mars"))

temperatures = """Saturn has a daytime temperature of -170 degrees Celsius, while Mars has -28 Celsius."""
print(temperatures.count("Mars"))
print(temperatures.count("Moon"))

print("The Moon And The Earth".lower())

temperatures = "Mars Average Temperature: -60 C"
temp=temperatures.split(":")
print(temp)
print(temp[-1])
