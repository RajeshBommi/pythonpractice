
 # verify email address
import re
def verify_email_address(email):
    if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
        return False
    return True

# verify phone number
def verify_phone_number(phone_number):
    if not re.match(r"^\d{10}$", phone_number):
        return False
    return True
    
# verify password
def verify_password(password):
    if not re.match(r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$", password):
        return False
    return True

# verify name
def verify_name(name):
    if not re.match(r"^[a-zA-Z]+$", name):
        return False
    return True

# verify age
def verify_age(age):
    if not re.match(r"^\d{2}$", age):
        return False
    return True

# verify gender
def verify_gender(gender):
    if gender not in ["male", "female"]:
        return False
    return True

# verify city
def verify_city(city):
    if not re.match(r"^[a-zA-Z]+$", city):
        return False
    return True


# write function to verify email address
def verify_email_address(email):
    if not re.match(r"[^@]+@[^@]+\.[^@]+", email):
        return False
    return True

# write function to verify phone number
def verify_phone_number(phone_number):
    if not re.match(r"^\d{10}$", phone_number):
        return False
    return True


# write function to verify password
def verify_password(password):
    if not re.match(r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8, }$", password):
        return False
    return True

# write a program to find the even numbers in a list
def find_even_numbers(numbers):
    even_numbers = []
    for number in numbers:
        if number % 2 == 0:
            even_numbers.append(number)
    return even_numbers
print(find_even_numbers([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))


