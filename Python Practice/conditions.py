a = 23
b = 13
if a < b:
    print(b)
else:
    print(a)

    c = 44
    d = 96
    if c <= d:
        print("c is lessthan d")
    elif c == d:
        print("both are equal")

e = 27
f = 93
if e > f:
    print("Greater")
elif f < e:
    print("lesser")
else:
    print("both are not equal")

    objectKeys = 10
    if objectKeys > 5:
        print("object is true")
    else:
        print("object is not true")

g = 12
h = 23
if g == 12 or h == 34:
    print(g + h)

g = 12
h = 23
if g == 12 and h == 23:
    print(g + h)

object_size = 10
proximity = 9000

if object_size > 5 and proximity < 1000:
    print("Evasive maneuvers required")
else:
    print("Object poses no threat")
