planets =["Mars","Earth","Jupiter"]
print(planets[0])
planets[0]="pluto"
print(planets)
no_of_planets=len(planets)
print(no_of_planets)
planets.append("venus")
print(planets)
no_of_planets=len(planets)
print(no_of_planets)
planets.pop()
print(planets)
print(planets[-1])

# Because indexing starts with 0, you need to add 1 to display the proper number.
n=planets.index("Jupiter")
print(n+1)