# first_planet=int(input("distance from the sun to Earth"))
# second_planet=int(input("distance from the sun to Jupiter"))

# distance_km=second_planet-first_planet
# print(distance_km)

# distance_ml=distance_km/1.609344
# print(distance_ml)

from math import ceil, floor

demo_int = int('215')
print(demo_int)

print(abs(39 - 16))
print(abs(16 - 39))

print(round(1.2))
print(round(2.9))
print(round(1.5))
print(round(2.5))

round_up = ceil(12.5)
print(round_up)

round_down = floor(12.5)
print(round_down)

x = "Python"
y = "is"
z = "awesome"

print(x, y, z)
