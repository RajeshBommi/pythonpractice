


# Create SES Client
import boto3
ses_client = boto3.client('ses')

# function to verify email address
def verify_email(email):
    response = ses_client.verify_email_address(
        EmailAddress=email
    )
    return response

